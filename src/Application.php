<?php

namespace Music\Api;

use Music\Api\Http\Request;
use Music\Api\Http\Response;
use Music\Api\Http\HttpError;

class Application
{
    private $container;

    public function __construct(array $config)
    {
        $this->container = $this->createContainer($config);
    }

    /**
     * Main method that handle the HTTP request to create
     * an HTTP response.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function handle(Request $request) :Response
    {
        try {
            $routing = $this->container['router']->match($request);

            $response = call_user_func_array($routing['controller'], $routing['parameters']);

            if (!$response instanceof Response) {
                $response = $this->container['view']->render($response);
            }
        } catch (HttpError $error) {
            $response = new Response(json_encode(['error' => $error->getMessage()]), $error->getCode(), ['Content-Type' => 'application/json']);
        } catch (\Exception $exception) {
            $response = new Response(json_encode(['error' => $exception->getMessage()]), 500, ['Content-Type' => 'application/json']);
        }

        return $response;
    }

    private function createContainer(array $config)
    {
        // This container could be replaced by Pimple to lazy-load services.
        $container = new \ArrayObject($config);

        // Database
        $container['db'] = call_user_func(function ($container) use ($config) {
            $db = new \PDO($config['database']['dsn'], $config['database']['username'], $config['database']['password']);
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $db;
        }, $container);

        // Model
        $container['repository.user'] = call_user_func(function ($container) {
            return new Repository\UserRepository($container['db']);
        }, $container);

        $container['repository.song'] = call_user_func(function ($container) {
            return new Repository\SongRepository($container['db']);
        }, $container);

        // Controllers
        $container['controller.user'] = call_user_func(function ($container) {
            return new Controller\UserController($container['repository.user'], $container['repository.song']);
        }, $container);

        $container['controller.song'] = call_user_func(function ($container) {
            return new Controller\ResourceController($container['repository.song']);
        }, $container);

        // View
        $container['view'] = call_user_func(function ($container) {
            return new View\JsonView();
        }, $container);

        // Router
        $container['router'] = call_user_func(function ($container) {
            return new Router($container);
        }, $container);

        return $container;
    }
}
