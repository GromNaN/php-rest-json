<?php

namespace Music\Api\Http;

/**
 * Should be replaced by Symfony Http Request.
 */
class Request
{
    private $uri;
    private $method;
    private $data;

    public function __construct($method, $uri, array $data = null)
    {
        $this->method = $method;
        $this->uri = $uri;
        $this->data = $data;
    }

    public static function createFromGlobals() :self
    {
        $contentType = isset($_SERVER['HTTP_CONTENT_TYPE']) ? $_SERVER['HTTP_CONTENT_TYPE'] : '';
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';

        $data = [];
        if ('application/json' === $contentType) {
            $data = json_decode(file_get_contents('php://input'), true);
        } elseif (in_array($method, ['POST', 'PUT', 'PATCH'], true)) {
            $data = $_POST;
        }

        return new static($method, $uri, $data);
    }

    public function getMethod() :string
    {
        return $this->method;
    }

    public function getUri() :string
    {
        return $this->uri;
    }

    public function getData() :array
    {
        return $this->data;
    }
}
