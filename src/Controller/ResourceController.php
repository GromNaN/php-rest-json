<?php

namespace Music\Api\Controller;

use Music\Api\Repository\ReadRepositoryInterface;
use Music\Api\Http\HttpError;

class ResourceController
{
    protected $repository;

    public function __construct(ReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function indexAction()
    {
        $objects = $this->repository->findAll();

        return [
            'data' => $objects,
        ];
    }

    public function getAction(int $id)
    {
        $object = $this->repository->find($id);

        if (!$object) {
            throw new HttpError('Object not found', 404);
        }

        return [
            'data' => $object,
        ];
    }
}
