<?php

namespace Music\Api\Controller;

use Music\Api\Http\Request;
use Music\Api\Http\Response;
use Music\Api\Http\HttpError;
use Music\Api\Model\User;
use Music\Api\Model\Song;
use Music\Api\Repository\UserRepository;
use Music\Api\Repository\SongRepository;

class UserController extends ResourceController
{
    private $songRepository;

    public function __construct(UserRepository $userRepository, SongRepository $songRepository)
    {
        parent::__construct($userRepository);

        $this->songRepository = $songRepository;
    }

    public function getFavoriteSongsAction(int $user_id)
    {
        $user = $this->getUser($user_id, 404);

        $songs = $this->repository->findFavoriteSongs($user);

        return [
            'data' => $songs,
        ];
    }

    public function postFavoriteSongAction(int $user_id, Request $request)
    {
        $user = $this->getUser($user_id, 404);

        $data = $request->getData();

        if (empty($data['song_id'])) {
            throw new HttpError('Expecting parameter "song_id".', 400);
        }

        $song = $this->getSong($data['song_id'], 422);

        $this->repository->saveFavoriteSong($user, $song);

        return new Response('', 201);
    }

    public function deleteFavoriteSongAction(int $user_id, int $song_id)
    {
        $user = $this->getUser($user_id, 404);
        $song = $this->getSong($song_id, 404);

        $this->repository->removeFavoriteSong($user, $song);

        return new Response('', 201);
    }

    private function getUser(int $user_id, int $errorStatus) :User
    {
        $user = $this->repository->find($user_id);

        if (!$user) {
            throw new HttpError('User not found.', $errorStatus);
        }

        return $user;
    }

    private function getSong(int $song_id, int $errorStatus) :Song
    {
        $song = $this->songRepository->find($song_id);

        if (!$song) {
            throw new HttpError(sprintf('Song "%s" not found.', $song_id), $errorStatus);
        }

        return $song;
    }
}
