<?php

namespace Music\Api;

use Music\Api\Http\Request;
use Music\Api\Http\HttpError;

class Router
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function match(Request $request) :array
    {
        $method = $request->getMethod();
        $uri = $request->getUri();

        if ('GET' === $method && preg_match('~^/users$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.user'], 'indexAction'],
                'parameters' => [],
            ];
        }

        if ('GET' === $method && preg_match('~^/users/(?P<user_id>\d++)$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.user'], 'getAction'],
                'parameters' => ['id' => $matches['user_id']],
            ];
        }

        if ('GET' === $method && preg_match('~^/songs$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.song'], 'indexAction'],
                'parameters' => [],
            ];
        }

        if ('GET' === $method && preg_match('~^/songs/(?P<song_id>\d++)$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.song'], 'getAction'],
                'parameters' => ['id' => $matches['song_id']],
            ];
        }

        if ('GET' === $method && preg_match('~^/users/(?P<user_id>\d++)/songs$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.user'], 'getFavoriteSongsAction'],
                'parameters' => ['user_id' => $matches['user_id']],
            ];
        }

        if ('POST' === $method && preg_match('~^/users/(?P<user_id>\d++)/songs$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.user'], 'postFavoriteSongAction'],
                'parameters' => ['user_id' => $matches['user_id'], 'request' => $request],
            ];
        }

        if ('DELETE' === $method && preg_match('~^/users/(?P<user_id>\d++)/songs/(?P<song_id>\d++)$~', $uri, $matches)) {
            return [
                'controller' => [$this->container['controller.user'], 'deleteFavoriteSongAction'],
                'parameters' => ['user_id' => $matches['user_id'], 'id' => $matches['song_id']],
            ];
        }

        throw new HttpError('Route not found.', 404);
    }
}
