<?php

namespace Music\Api\Model;

class User implements \JsonSerializable
{
    private $id;

    private $name;

    private $email;

    public function getId() :int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName() :string
    {
        return $this->name;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getEmail() :string
    {
        return $this->email;
    }

    public function jsonSerialize() :array
    {
        return [
            'user_id' => (int) $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }

    public static function __set_state($values)
    {
        $object = new static();
        foreach ($values as $key => $value) {
            $object->{$key} = $value;
        }

        return $object;
    }
}
