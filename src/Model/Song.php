<?php

namespace Music\Api\Model;

class Song implements \JsonSerializable
{
    private $id;

    private $title;

    private $duration;

    public function getId() :int
    {
        return $this->id;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle() :string
    {
        return $this->title;
    }

    public function setDuration(int $duration)
    {
        $this->duration = $duration;
    }

    public function getDuration() :int
    {
        return $this->duration;
    }

    public function jsonSerialize() :array
    {
        return [
            'song_id' => (int) $this->id,
            'title' => $this->title,
            'duration' => (int) $this->duration,
        ];
    }

    public static function __set_state($values)
    {
        $object = new static();
        foreach ($values as $key => $value) {
            $object->{$key} = $value;
        }

        return $object;
    }
}
