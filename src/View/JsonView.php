<?php

namespace Music\Api\View;

use Music\Api\Http\Response;

class JsonView
{
    public function render(array $controllerResult) :Response
    {
        if (!isset($controllerResult['data'])) {
            throw new \InvalidArgumentException('Controller must return a "data" value.');
        }

        // JMS Serializer could be used here to get a more configurable representation.
        $content = json_encode($controllerResult['data'], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);

        $headers = [
            'Content-Type' => 'application/json',
        ];

        return new Response($content, 200, $headers);
    }
}
