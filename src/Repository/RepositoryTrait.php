<?php

namespace Music\Api\Repository;

/**
 * Implements methods of ReadRepositoryInterface.
 */
trait RepositoryTrait
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function findAll() :array
    {
        $stmt = $this->connection->prepare(sprintf('SELECT * FROM %s', static::TABLE_NAME));

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, static::MODEL_CLASS);
    }

    public function find(int $id)
    {
        $stmt = $this->connection->prepare(sprintf('SELECT * FROM %s WHERE %s = :id LIMIT 1', static::TABLE_NAME, static::PRIMARY_KEY));

        $stmt->execute([':id' => $id]);

        return $stmt->fetchObject(static::MODEL_CLASS);
    }
}
