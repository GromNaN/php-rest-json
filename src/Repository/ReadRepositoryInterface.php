<?php

namespace Music\Api\Repository;

interface ReadRepositoryInterface
{
    public function findAll() :array;

    public function find(int $id);
}
