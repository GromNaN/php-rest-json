<?php

namespace Music\Api\Repository;

class SongRepository implements ReadRepositoryInterface
{
    const TABLE_NAME = 'song';
    const PRIMARY_KEY = 'id';
    const MODEL_CLASS = 'Music\Api\Model\Song';

    use RepositoryTrait;
}
