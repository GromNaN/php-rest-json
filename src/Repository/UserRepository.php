<?php

namespace Music\Api\Repository;

use Music\Api\Model\User;
use Music\Api\Model\Song;

class UserRepository implements ReadRepositoryInterface
{
    const TABLE_NAME = 'user';
    const PRIMARY_KEY = 'id';
    const MODEL_CLASS = 'Music\Api\Model\User';

    use RepositoryTrait;

    const FAVORITE_SONG_TABLE_NAME = 'user_fav_song';

    public function findFavoriteSongs(User $user)
    {
        $stmt = $this->connection->prepare(sprintf(
            'SELECT * FROM %s s INNER JOIN %s f ON s.id = f.song_id WHERE f.user_id = :user_id',
            SongRepository::TABLE_NAME,
            static::FAVORITE_SONG_TABLE_NAME
        ));

        $stmt->execute(['user_id' => $user->getId()]);

        return $stmt->fetchAll(\PDO::FETCH_CLASS, SongRepository::MODEL_CLASS);
    }

    public function saveFavoriteSong(User $user, Song $song)
    {
        $stmt = $this->connection->prepare(sprintf('REPLACE INTO %s (user_id, song_id) VALUES (:user_id, :song_id)', static::FAVORITE_SONG_TABLE_NAME));

        $stmt->execute([
            'user_id' => $user->getId(),
            'song_id' => $song->getId(),
        ]);

        return $stmt->rowCount() > 0;
    }

    public function removeFavoriteSong(User $user, Song $song)
    {
        $stmt = $this->connection->prepare(sprintf('DELETE FROM %s WHERE user_id = :user_id AND song_id = :song_id', static::FAVORITE_SONG_TABLE_NAME));

        $stmt->execute([
            'user_id' => $user->getId(),
            'song_id' => $song->getId(),
        ]);

        return $stmt->rowCount() > 0;
    }
}
