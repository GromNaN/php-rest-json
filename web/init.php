<?php

$config = parse_ini_file(__DIR__.'/../config/parameters.ini', true);

$db = new \PDO($config['database']['dsn'], $config['database']['username'], $config['database']['password']);
$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

$db->exec(file_get_contents(__DIR__.'/../resources/schema.sql'));
$db->exec(file_get_contents(__DIR__.'/../resources/fixtures.sql'));
