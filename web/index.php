<?php

require __DIR__.'/../vendor/autoload.php';

$config = parse_ini_file(__DIR__.'/../config/parameters.ini', true);
$app = new Music\Api\Application($config);

$request = Music\Api\Http\Request::createFromGlobals();
$response = $app->handle($request);
$response->send();
