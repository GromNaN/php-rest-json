composer.phar:
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('SHA384', 'composer-setup.php') === trim(file_get_contents('https://composer.github.io/installer.sig'))) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php --quiet
	php -r "unlink('composer-setup.php');"

phpunit.phar:
	php -r "copy('https://phar.phpunit.de/phpunit-6.2.phar', 'phpunit.phar'); chmod('phpunit.phar', 0755);"
	php phpunit.phar --version

php-cs-fixer.phar:
	php -r "copy('http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar', 'php-cs-fixer.phar'); chmod('php-cs-fixer.phar', 0755);"
	php php-cs-fixer.phar --version

vendor: composer.phar
	php composer.phar install

test: vendor phpunit.phar
	php phpunit.phar

test-cs: php-cs-fixer.phar
	php php-cs-fixer.phar fix --dry-run

fix-cs: php-cs-fixer.phar
	php php-cs-fixer.phar fix

serve: vendor
	php -S localhost:8080 -t web web/index.php

init-db: vendor
	php web/init.php
