Music API example
=================

This is a sample REST API developed from scratch without any external dependency.

Install
-------

    git clone https://gitlab.com/GromNaN/php-rest-json.git php-rest-json
    cd php-rest-json

Edit database configuration in `config/parameters.ini`.
SQLite is used by default, but you can user MySQL.

    make init-db

Running development server
--------------------------

For development, run the PHP internal server:

    make serve

The API is accessible at [http://localhost:8080/](http://localhost:8080/users).

Running tests
-------------

Tests requires PHPUnit 4.1+

    make test

API methods
===========

Getting users
-------------

    $ curl -XGET http://localhost:8080/users

    [
        {
            "user_id": 1,
            "name": "Jacques",
            "email": "jacques@company.com"
        },
        {
            "user_id": 2,
            "name": "Henri",
            "email": "henri@company.com"
        },
        {
            "user_id": 3,
            "name": "Marie",
            "email": "marie@company.com"
        }
    ]

Getting songs
-------------

    $ curl -XGET http://localhost:8080/songs

    [
        {
            "song_id": 1,
            "title": "Freedom",
            "duration": 354
        },
        {
            "song_id": 2,
            "title": "Show Me",
            "duration": 652
        },
        {
            "song_id": 3,
            "title": "Forgiveness",
            "duration": 234
        },
        {
            "song_id": 4,
            "title": "Communion",
            "duration": 524
        }
    ]

Getting favorite songs
----------------------

    $ curl -XGET http://localhost:8080/users/1/songs

    [
        {
            "song_id": 1,
            "title": "Freedom",
            "duration": 354
        },
        {
            "song_id": 3,
            "title": "Forgiveness",
            "duration": 234
        },
        {
            "song_id": 4,
            "title": "Communion",
            "duration": 524
        }
    ]

Adding a favorite song
----------------------

    $ curl -XPOST http://localhost:8080/users/1/songs -H 'Content-Type: application/json' -d '{"song_id":2}'

Removing a favorite song
------------------------

    $ curl -XDELETE http://localhost:8080/users/1/songs/3 --verbose

Copyright
=========

This code is authored by Jérôme TAMARELLE and shared for demonstration only.
The author explicitly prohibits reuse of this code.
