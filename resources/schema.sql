
DROP TABLE IF EXISTS user_fav_song;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS song;

CREATE TABLE user (
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	email VARCHAR(200) NOT NULL
);

CREATE TABLE song (
	id INTEGER NOT NULL PRIMARY KEY,
	title VARCHAR(200) NOT NULL,
	duration INTEGER NULL
);

CREATE TABLE user_fav_song (
	user_id INTEGER NOT NULL,
	song_id INTEGER NOT NULL,
	PRIMARY KEY (user_id, song_id),
	CONSTRAINT fk_user_fav_song_user_id FOREIGN KEY (user_id) REFERENCES user(id),
	CONSTRAINT fk_user_fav_song_song_id FOREIGN KEY (song_id) REFERENCES song(id)
);
