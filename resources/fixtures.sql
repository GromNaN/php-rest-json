INSERT INTO user (id, name, email) VALUES (1, 'Jacques', 'jacques@company.com');
INSERT INTO user (id, name, email) VALUES (2, 'Henri', 'henri@company.com');
INSERT INTO user (id, name, email) VALUES (3, 'Marie', 'marie@company.com');

INSERT INTO song (id, title, duration) VALUES (1, 'Freedom', 354);
INSERT INTO song (id, title, duration) VALUES (2, 'Show Me', 652);
INSERT INTO song (id, title, duration) VALUES (3, 'Forgiveness', 234);
INSERT INTO song (id, title, duration) VALUES (4, 'Communion', 524);

INSERT INTO user_fav_song (user_id, song_id) VALUES (1, 1);
INSERT INTO user_fav_song (user_id, song_id) VALUES (1, 3);
INSERT INTO user_fav_song (user_id, song_id) VALUES (1, 4);
INSERT INTO user_fav_song (user_id, song_id) VALUES (2, 3);
