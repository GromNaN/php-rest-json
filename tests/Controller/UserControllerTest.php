<?php

namespace Music\Api\Tests\Controller;

use Music\Api\Model\User;
use Music\Api\Model\Song;
use Music\Api\Controller\UserController;
use Music\Api\Http\Request;
use Music\Api\Http\Response;
use Music\Api\Repository\UserRepository;
use Music\Api\Repository\SongRepository;
use PHPUnit\Framework\TestCase;

class UserControllerTest extends TestCase
{
    public function test_getFavoriteSongs_should_return_a_list_of_songs()
    {
        $user = new User();
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn($user);
        $repository->findFavoriteSongs($user)->willReturn([new \stdClass(), new \stdClass(), new \stdClass()]);

        $songRepository = $this->prophesize(SongRepository::class);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());
        $result = $controller->getFavoriteSongsAction(2);

        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
    }

    /**
     * @expectedException Music\Api\Http\HttpError
     * @expectedExceptionMessage User not found
     */
    public function test_getFavoriteSongs_fail_if_user_does_not_exist()
    {
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn(false);

        $songRepository = $this->prophesize(SongRepository::class);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());

        $controller->getFavoriteSongsAction(2);
    }

    public function test_postFavoriteSong_should_save_song()
    {
        $user = new User();
        $song = new Song();
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn($user);
        $repository->saveFavoriteSong($user, $song)->willReturn(true);

        $songRepository = $this->prophesize(SongRepository::class);
        $songRepository->find(3)->willReturn($song);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());

        $request = new Request('GET', '/', ['song_id' => 3]);
        $result = $controller->postFavoriteSongAction(2, $request);

        $this->assertInstanceOf(Response::class, $result);
    }

    /**
     * @expectedException Music\Api\Http\HttpError
     * @expectedExceptionMessage User not found
     */
    public function test_postFavoriteSong_fail_if_user_does_not_exist()
    {
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn(false);

        $songRepository = $this->prophesize(SongRepository::class);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());

        $request = new Request('GET', '/', ['song_id' => 3]);
        $controller->postFavoriteSongAction(2, $request);
    }

    /**
     * @expectedException Music\Api\Http\HttpError
     * @expectedExceptionMessage Song "3" not found.
     */
    public function test_postFavoriteSong_fail_if_song_does_not_exist()
    {
        $user = new User();
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn($user);

        $songRepository = $this->prophesize(SongRepository::class);
        $songRepository->find(3)->willReturn(false);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());

        $request = new Request('GET', '/', ['song_id' => 3]);
        $controller->postFavoriteSongAction(2, $request);
    }

    public function test_deleteFavoriteSong_should_remove_song()
    {
        $user = new User();
        $song = new Song();
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn($user);
        $repository->removeFavoriteSong($user, $song)->willReturn(true);

        $songRepository = $this->prophesize(SongRepository::class);
        $songRepository->find(3)->willReturn($song);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());

        $result = $controller->deleteFavoriteSongAction(2, 3);

        $this->assertInstanceOf(Response::class, $result);
    }

    /**
     * @expectedException Music\Api\Http\HttpError
     * @expectedExceptionMessage User not found
     */
    public function test_deleteFavoriteSong_fail_if_user_does_not_exist()
    {
        $repository = $this->prophesize(UserRepository::class);
        $repository->find(2)->willReturn(false);

        $songRepository = $this->prophesize(SongRepository::class);

        $controller = new UserController($repository->reveal(), $songRepository->reveal());

        $request = new Request('GET', '/', ['song_id' => 3]);
        $controller->deleteFavoriteSongAction(2, 5);
    }
}
