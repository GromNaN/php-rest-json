<?php

namespace Music\Api\Tests\Controller;

use Music\Api\Controller\ResourceController;
use Music\Api\Repository\ReadRepositoryInterface;
use PHPUnit\Framework\TestCase;

class ResourceControllerTest extends TestCase
{
    public function test_index_should_return_all_objects()
    {
        $repository = $this->prophesize(ReadRepositoryInterface::class);
        $repository->findAll()->willReturn([new \stdClass(), new \stdClass(), new \stdClass()]);

        $controller = new ResourceController($repository->reveal());
        $result = $controller->indexAction();

        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
    }

    public function test_get_should_return_one_object()
    {
        $repository = $this->prophesize(ReadRepositoryInterface::class);
        $repository->find(10)->willReturn(new \stdClass());

        $controller = new ResourceController($repository->reveal());
        $result = $controller->getAction(10);

        $this->assertArrayHasKey('data', $result);
        $this->assertInstanceOf(\stdClass::class, $result['data']);
    }
}
