<?php

namespace Music\Api\Tests\View;

use Music\Api\View\JsonView;
use PHPUnit\Framework\TestCase;

class JsonViewTest extends TestCase
{
    public function test_render_should_encode_to_json()
    {
        $view = new JsonView();

        $response = $view->render([
            'data' => ['this' => 'is data'],
        ]);

        $this->assertEquals(['this' => 'is data'], json_decode($response->getContent(), true));
        $this->assertEquals('application/json', $response->getHeaders()['Content-Type']);
    }
}
