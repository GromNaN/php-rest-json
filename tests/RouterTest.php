<?php

namespace Music\Api\Tests;

use Music\Api\Router;
use Music\Api\Http\Request;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    /**
     * @dataProvider getRoutesWithParameters
     */
    public function test_match_should_return_parameters(Request $request, array $expected)
    {
        $router = new Router([
            'controller.user' => 'controller.user',
            'controller.song' => 'controller.song',
        ]);

        $this->assertEquals($expected, $router->match($request));
    }

    public function getRoutesWithParameters()
    {
        return [
            [
                new Request('GET', '/users/1'),
                [
                    'controller' => ['controller.user', 'getAction'],
                    'parameters' => ['id' => 1],
                ],
            ],
            [
                new Request('GET', '/users'),
                [
                    'controller' => ['controller.user', 'indexAction'],
                    'parameters' => [],
                ],
            ],
            [
                new Request('GET', '/songs/1'),
                [
                    'controller' => ['controller.song', 'getAction'],
                    'parameters' => ['id' => 1],
                ],
            ],
            [
                new Request('GET', '/songs'),
                [
                    'controller' => ['controller.song', 'indexAction'],
                    'parameters' => [],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getRoutesNotFound
     *
     * @expectedException Music\Api\Http\HttpError
     */
    public function test_match_shoud_throw_http_error(Request $request)
    {
        $router = new Router([
            'controller.user' => 'controller.user',
            'controller.song' => 'controller.song',
        ]);

        $router->match($request);
    }

    public function getRoutesNotFound()
    {
        return [
            [
                new Request('GET', ''),
            ],
            [
                new Request('GET', '/users/'),
            ],
            [
                new Request('GET', '/songs/abc'),
            ],
            [
                new Request('GET', '/users/10/'),
            ],
        ];
    }
}
