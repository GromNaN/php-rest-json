<?php

namespace Music\Api\Tests\Repository;

use Music\Api\Model\User;
use Music\Api\Model\Song;
use Music\Api\Repository\UserRepository;
use PHPUnit\Framework\TestCase;

class UserRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $connection = new \PDO('sqlite::memory:');
        $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $connection->exec(file_get_contents(__DIR__.'/../../resources/schema.sql'));
        $connection->exec(file_get_contents(__DIR__.'/../../resources/fixtures.sql'));

        $this->repository = new UserRepository($connection);
    }

    public function test_findAll_should_return_an_array_of_objects()
    {
        $songs = $this->repository->findAll();

        $this->assertCount(3, $songs);
        $this->assertInstanceOf(User::class, $songs[0]);
    }

    public function test_find_should_return_an_object()
    {
        $song = $this->repository->find(2);

        $this->assertInstanceOf(User::class, $song);
        $this->assertEquals(2, $song->getId());
    }

    public function test_find_should_return_false_for_entry_not_found()
    {
        $song = $this->repository->find(1000);

        $this->assertFalse($song);
    }

    public function test_findFavoriteSongs_should_return_an_array_of_songs()
    {
        $user = User::__set_state(['id' => 1]);

        $songs = $this->repository->findFavoriteSongs($user);

        $this->assertInternalType('array', $songs);
        $this->assertCount(3, $songs);
        $this->assertInstanceOf(Song::class, $songs[0]);
        $this->assertEquals(1, $songs[0]->getId());
        $this->assertEquals(3, $songs[1]->getId());
        $this->assertEquals(4, $songs[2]->getId());
    }

    public function test_findFavoriteSongs_should_return_an_empty_array_of_songs()
    {
        $user = User::__set_state(['id' => 3]);

        $songs = $this->repository->findFavoriteSongs($user);

        $this->assertInternalType('array', $songs);
        $this->assertCount(0, $songs);
    }

    public function test_saveFavoriteSong_should_add_a_new_song()
    {
        $user = User::__set_state(['id' => 4]);
        $song = Song::__set_state(['id' => 3]);

        $result = $this->repository->saveFavoriteSong($user, $song);
        $this->assertTrue($result);

        $songs = $this->repository->findFavoriteSongs($user);

        $this->assertCount(1, $songs);
        $this->assertEquals($song->getId(), $songs[0]->getId());
    }

    public function test_saveFavoriteSong_should_keep_an_existing_song()
    {
        $user = User::__set_state(['id' => 1]);
        $song = Song::__set_state(['id' => 3]);

        $result = $this->repository->saveFavoriteSong($user, $song);
        $this->assertTrue($result);

        $songs = $this->repository->findFavoriteSongs($user);

        $this->assertCount(3, $songs);
    }

    public function test_removeFavoriteSong_should_delete_a_song()
    {
        $user = User::__set_state(['id' => 1]);
        $song = Song::__set_state(['id' => 3]);

        $result = $this->repository->removeFavoriteSong($user, $song);
        $this->assertTrue($result);

        $songs = $this->repository->findFavoriteSongs($user);

        $this->assertCount(2, $songs);
        foreach ($songs as $song) {
            $this->assertNotEquals(3, $song->getId());
        }
    }

    public function test_removeFavoriteSong_should_let_not_existing_song()
    {
        $user = User::__set_state(['id' => 3]);
        $song = Song::__set_state(['id' => 4]);

        $result = $this->repository->removeFavoriteSong($user, $song);
        $this->assertFalse($result);
    }
}
