<?php

namespace Music\Api\Tests\Repository;

use Music\Api\Repository\SongRepository;
use PHPUnit\Framework\TestCase;

class SongRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $connection = new \PDO('sqlite::memory:');

        $connection->exec(file_get_contents(__DIR__.'/../../resources/schema.sql'));
        $connection->exec(file_get_contents(__DIR__.'/../../resources/fixtures.sql'));

        $this->repository = new SongRepository($connection);
    }

    public function test_find_all_should_return_an_array_of_objects()
    {
        $songs = $this->repository->findAll();

        $this->assertCount(4, $songs);
        $this->assertInstanceOf(SongRepository::MODEL_CLASS, $songs[0]);
    }

    public function test_find_should_return_an_object()
    {
        $song = $this->repository->find(2);

        $this->assertInstanceOf(SongRepository::MODEL_CLASS, $song);
        $this->assertEquals(2, $song->getId());
    }

    public function test_find_should_return_false_for_entry_not_found()
    {
        $song = $this->repository->find(1000);

        $this->assertFalse($song);
    }
}
